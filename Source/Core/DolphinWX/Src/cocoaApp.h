#import <Cocoa/Cocoa.h>

#ifdef __cplusplus
extern "C"
{
#endif

bool cocoaSendEvent(NSEvent *event);

void cocoaCreateApp();

#ifdef __cplusplus
}
#endif

