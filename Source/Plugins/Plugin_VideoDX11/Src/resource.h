//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by Resource.rc
//
#define IDD_ABOUT                       102
#define IDD_SETTINGS                    103
#define IDD_ADVANCED                    105
#define IDC_ADAPTER                     1001
#define IDC_VSYNC                       1006
#define IDC_ASPECT_16_9                 1008
#define IDC_ASPECT_4_3                  1009
#define IDC_WIDESCREEN_HACK             1010
#define IDC_SAFE_TEXTURE_CACHE          1011
#define IDC_EFB_ACCESS_ENABLE           1012
#define IDC_WIREFRAME                   1013
#define IDC_DISABLEFOG                  1014
#define IDC_OVERLAYFPS                  1015
#define IDC_OVERLAYSTATS                1016
#define IDC_OVERLAYPROJSTATS            1017
#define IDC_ENABLEEFBCOPY               1018
#define IDC_TEXFMT_OVERLAY              1024
#define IDC_TEXFMT_CENTER               1025
#define IDC_ENABLEXFB                   1026
#define IDC_FORCEANISOTROPY             1027
#define IDC_EFBSCALEDCOPY               1029
#define IDC_OSDHOTKEY                   1030
#define IDC_ASPECTRATIO                 1040
#define IDC_SAFE_TEXTURE_CACHE_SAFE     1041
#define IDC_SAFE_TEXTURE_CACHE_NORMAL   1042
#define IDC_SAFE_TEXTURE_CACHE_FAST     1043
#define IDC_DLIST_CACHING               1044
#define IDC_ENABLEPIXELLIGHTING         1045
#define IDC_LOADHIRESTEXTURE            1046
#define IDC_DUMPTEXTURES                1047
#define IDC_INTERNALRESOLUTION			1048
#define IDC_STATIC                      -1

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        106
#define _APS_NEXT_COMMAND_VALUE         40001
#define _APS_NEXT_CONTROL_VALUE         1046
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
